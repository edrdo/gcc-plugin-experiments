
int foo_int(int x, int y) {
  int r = 0;
  if (x == y) r++;
  if (x != y) r++;
  if (x < y) r++;
  if (x > y) r++;
  if (x >= y) r++;
  if (x <= y) r++;
  if (x) r++;
  if (! x) r++;
  return r;
}
int main(int argc, char** argv) {
  int x = 1, y = 3;

  if (x == y) {
    printf("Impossible!\n");
  }
}
#if 0
char foo_char(char x, char y) {
  char r = 0;
  if (x == y) r++;
  if (x != y) r++;
  if (x < y) r++;
  if (x > y) r++;
  if (x >= y) r++;
  if (x <= y) r++;
  if (x) r++;
  if (! x) r++;
  return r++;
}
char foo_char_p(char* x, char* y) {
  char r = 0;
  if (x == y) r++;
  if (x != y) r++;
  if (x < y) r++;
  if (x > y) r++;
  if (x >= y) r++;
  if (x <= y) r++;
  if (x) r++;
  if (! x) r++;
  return r++;
}

char foo_char_pp(char** x, char** y) {
  char r = 0;
  if (x == y) r++;
  if (x != y) r++;
  if (x < y) r++;
  if (x > y) r++;
  if (x >= y) r++;
  if (x <= y) r++;
  if (x) r++;
  if (! x) r++;
  return r++;
}
#endif
