#include <iostream>

// GCC PLUGIN HEADERS
//#include <gcc-plugin.h>
//#include <plugin-version.h>

//#include <tree-pass.h>
//#include <context.h>
//#include <basic-block.h>

//#include <gimple-iterator.h>
//#include <gimple-pretty-print.h>
#include "gcc-plugin.h"
#include "plugin-version.h"

#include "tree-pass.h"
#include "context.h"
#include "function.h"
#include "tree.h"
#include "tree-ssa-alias.h"
#include "internal-fn.h"
#include "is-a.h"
#include "predict.h"
#include "basic-block.h"
#include "gimple-expr.h"
#include "gimple.h"
#include "gimple-pretty-print.h"
#include "gimple-iterator.h"
#include "gimple-walk.h"
// #include "plugin.def"
// #include "gimple.def"

int plugin_is_GPL_compatible = 1;

static ::plugin_info gPluginInfo = { .version = "0.1", .help="Print GIMPLE" };


static const ::pass_data gPassData =
{
  GIMPLE_PASS,
  "PrintGIMPLE",	/* name */
  OPTGROUP_NONE,	/* optinfo_flags */
  TV_NONE,		/* tv_id */
  PROP_gimple_any,	/* properties_required */
  0,			/* properties_provided */
  0,			/* properties_destroyed */
  0,			/* todo_flags_start */
  0			/* todo_flags_finish */
};

inline std::ostream& operator+(std::ostream& os, location_t loc) {
  return os << (LOCATION_FILE(loc) ? : "???")
            << ":" << LOCATION_LINE(loc);
}

#define DEFGSCODE(SYM, STRING, TYPE) #SYM,
const char* OPCODES[] = {
  #include "gimple.def"
};

static const int NUMBER_OF_OPCODES = sizeof(OPCODES) / sizeof(OPCODES[0]);


#define DEFTREECODE(SYM, STRING, TYPE, NARGS)   #SYM ,
const char* TREECODES[] = {
  #include "tree.def"
};
static const int NUMBER_OF_TREECODES = sizeof(TREECODES) / sizeof(TREECODES[0]);


struct CompilerPass : gimple_opt_pass {

  CompilerPass() : gimple_opt_pass(gPassData, g) {

  }
  static const char* locationInfo(location_t loc) {
    return LOCATION_FILE(loc) ? : "<unknown>";
  }

  unsigned int execute(function * fun)  {
    basic_block bb;

    std::cerr + fun->function_start_locus << ' ' << function_name(fun) << " { " << std::endl;

    FOR_ALL_BB_FN(bb, fun)
    {
      gimple_bb_info *bb_info = &bb->il.gimple;

      for (gimple_stmt_iterator gsi = gsi_start (bb_info->seq); !gsi_end_p (gsi); gsi_next (&gsi) ) {

        gimple stmt = gsi_stmt (gsi);

        if (gimple_code(stmt) == GIMPLE_COND) {
          tree_code op = gimple_cond_code(stmt);
          tree lhs = gimple_cond_lhs(stmt);
          tree rhs = gimple_cond_rhs(stmt);
          tree type = TREE_TYPE(lhs);
          bool isPointerExpr = type->base.code == POINTER_TYPE;

          std::string njcFuncName("NJC_");
          njcFuncName += TREECODES[op];
          njcFuncName += "_";
          njcFuncName += isPointerExpr ? "pointer" : IDENTIFIER_POINTER(DECL_NAME(TYPE_NAME(type)));

          tree args = build_function_type_list(integer_type_node, type, type, NULL_TREE);
          tree fn = build_fn_decl(njcFuncName.c_str(), args);
          tree tmpVar = create_tmp_var(integer_type_node, "__NJC__");
          //add_referenced_var(tmpVar);
          //mark_sym_for_renaming(tmpVar);

          gcall* callInstr = gimple_build_call(fn, 2, lhs, rhs);
          gimple_call_set_lhs(callInstr, tmpVar);
          gsi_insert_before(&gsi, callInstr, GSI_SAME_STMT);
          gimple_set_location(callInstr, stmt->location);
          gimple_cond_set_code((gcond*)stmt, EQ_EXPR);
          gimple_cond_set_lhs((gcond*)stmt, tmpVar);
          gimple_cond_set_rhs((gcond*) stmt, build_int_cst (integer_type_node, 1));
//          build_call_expr()
//          tree var = create_tmp_var(TREE_TYPE(lhs), "dummy_var");

          (std::cerr + stmt->location) << ' ' << njcFuncName << std::endl;
        }
      }
    }

    FOR_ALL_BB_FN(bb, fun)
    {
      std::cerr << "  BB_" << bb->index << " { " << std::endl;
      gimple_bb_info *bb_info = &bb->il.gimple;

      for (gimple_stmt_iterator gsi = gsi_start (bb_info->seq); !gsi_end_p (gsi); gsi_next (&gsi) ) {

        gimple stmt = gsi_stmt (gsi);

        (std::cerr << "    " ) + stmt->location << ' '
                                                <<  gimple_code(stmt) << ':'
                                                 << OPCODES[gimple_code(stmt)] << ':'
                                                 << "ops=" << gimple_num_ops(stmt) <<  ':'
                                                 << "treeCode=" << TREECODES[gimple_expr_code(stmt)]
                                                 << std::endl;
        ::print_gimple_stmt(stderr, stmt, 0, 0);
      }

      edge e;
      edge_iterator ei;

      FOR_EACH_EDGE(e, ei, bb->succs)
      {
        std::cerr << "    --> BB_" << e->dest->index << std::endl;
      }
      std::cerr << "  }" << std::endl;

    }

    std::cerr + fun->function_end_locus << " }" << std::endl;
    std::cerr.flush();
    // Nothing special todo
    return 0;
  }

  bool gate(function *fun) {
    return true;
  }

  CompilerPass* clone() {
    return this;
  }
};

int plugin_init(struct plugin_name_args   *args,  /* Argument information */
                struct plugin_gcc_version *version)   /* Version info of GCC  */
{
  if (! ::plugin_default_version_check (version, &gcc_version)) {
    return 1;
  }

  ::register_callback(args->base_name, PLUGIN_INFO, NULL, &gPluginInfo);
  ::register_pass_info pass_info;

  pass_info.pass = new CompilerPass(); // "g" is a global gcc::context pointer
  pass_info.reference_pass_name = "cfg";
  pass_info.ref_pass_instance_number = 1;
  pass_info.pos_op = PASS_POS_INSERT_AFTER;

  ::register_callback(args->base_name, PLUGIN_PASS_MANAGER_SETUP, NULL, &pass_info);

  return 0;
}



