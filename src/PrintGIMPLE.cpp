#include <iostream>

// GCC PLUGIN HEADERS
//#include <gcc-plugin.h>
//#include <plugin-version.h>

//#include <tree-pass.h>
//#include <context.h>
//#include <basic-block.h>

//#include <gimple-iterator.h>
//#include <gimple-pretty-print.h>
#include "gcc-plugin.h"
#include "plugin-version.h"

#include "tree-pass.h"
#include "context.h"
#include "function.h"
#include "tree.h"
#include "tree-ssa-alias.h"
#include "internal-fn.h"
#include "is-a.h"
#include "predict.h"
#include "basic-block.h"
#include "gimple-expr.h"
#include "gimple.h"
#include "gimple-pretty-print.h"
#include "gimple-iterator.h"
#include "gimple-walk.h"


int plugin_is_GPL_compatible = 1;

static ::plugin_info gPluginInfo = { .version = "0.1", .help="Print GIMPLE" };


static const ::pass_data gPassData =
{
  GIMPLE_PASS,
  "PrintGIMPLE",	/* name */
  OPTGROUP_NONE,	/* optinfo_flags */
  TV_NONE,		/* tv_id */
  PROP_gimple_any,	/* properties_required */
  0,			/* properties_provided */
  0,			/* properties_destroyed */
  0,			/* todo_flags_start */
  0			/* todo_flags_finish */
};

inline std::ostream& operator+(std::ostream& os, location_t loc) {
  return os << (LOCATION_FILE(loc) ? : "???")
            << ":" << LOCATION_LINE(loc);
}

struct CompilerPass : gimple_opt_pass {

  CompilerPass() : gimple_opt_pass(gPassData, g) {

  }
  static const char* locationInfo(location_t loc) {
    return LOCATION_FILE(loc) ? : "<unknown>";
  }

  unsigned int execute(function * fun)  {
    basic_block bb;

    std::cerr + fun->function_start_locus << ' ' << function_name(fun) << " { " << std::endl;

    FOR_ALL_BB_FN(bb, fun)
    {
      gimple_bb_info *bb_info = &bb->il.gimple;

      std::cerr << "  BB_" << bb->index << " { " << std::endl;
      for (::gimple_stmt_iterator gsi = gsi_start (bb_info->seq); !gsi_end_p (gsi); gsi_next (&gsi)) {
        gimple stmt = gsi_stmt (gsi);
        (std::cerr << "    " ) + stmt->location << ' ';
        ::print_gimple_stmt(stderr, stmt, 0, 0);
      }

      edge e;
      edge_iterator ei;

      FOR_EACH_EDGE(e, ei, bb->succs)
      {
        std::cerr << "    --> BB_" << e->dest->index << std::endl;
      }
      std::cerr << "  }" << std::endl;

    }

    std::cerr + fun->function_end_locus << " }" << std::endl;
    std::cerr.flush();
    // Nothing special todo
    return 0;
  }

  bool gate(function *fun) {
    return true;
  }

  CompilerPass* clone() {
    return this;
  }
};

int plugin_init(struct plugin_name_args   *args,  /* Argument information */
                struct plugin_gcc_version *version)   /* Version info of GCC  */
{
  if (! ::plugin_default_version_check (version, &gcc_version)) {
    return 1;
  }

  ::register_callback(args->base_name, PLUGIN_INFO, NULL, &gPluginInfo);
  ::register_pass_info pass_info;

  pass_info.pass = new CompilerPass(); // "g" is a global gcc::context pointer
  pass_info.reference_pass_name = "cfg";
  pass_info.ref_pass_instance_number = 1;
  pass_info.pos_op = PASS_POS_INSERT_AFTER;

  ::register_callback(args->base_name, PLUGIN_PASS_MANAGER_SETUP, NULL, &pass_info);

  return 0;
}



