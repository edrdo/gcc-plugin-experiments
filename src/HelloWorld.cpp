#include <iostream>

// GCC PLUGIN HEADERS
#include <gcc-plugin.h>
#include <plugin-version.h> // Required for gcc_version symbol

int plugin_is_GPL_compatible = 1;  

int plugin_init(struct plugin_name_args   *info,  /* Argument information */
                struct plugin_gcc_version *version)   /* Version info of GCC  */
{
  if (!plugin_default_version_check (version, &gcc_version))
    return 1;

  std::cout << "Hello world!" << std::endl
            << "pl base name: " << info->base_name << std::endl
            << "pl full name: " << info->full_name << std::endl
            << "pl argc: "      << info->argc      << std::endl
            << "gcc basever: "  << version->basever    << std::endl ;
  std::cout.flush();
  
  return 0;
}

