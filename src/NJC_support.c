#include <stdio.h>
int NJC_EQ_EXPR_int(int x, int y) {
  printf("mutated %s\n", __func__);
  return ! ( x == y); 
}
int NJC_NE_EXPR_int(int x, int y) {
  return ! ( x != y); 
}
int NJC_LT_EXPR_int(int x, int y) {
  return ! ( x < y); 
}
int NJC_LE_EXPR_int(int x, int y) {
  return ! ( x <= y); 
}
int NJC_GT_EXPR_int(int x, int y) {
  return ! ( x > y); 
}
int NJC_GE_EXPR_int(int x, int y) {
  return ! ( x >= y); 
}

