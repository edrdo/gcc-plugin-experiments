
int foo(int x, int y) {

  if (x > 0) {
   return x;
  }

  if (x > y) {
   x = x*x;
   y = x;
  }
  
  return x + y;

}
